/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.boat.platform.test.dao;

import com.boat.platform.common.persistence.CrudDao;
import com.boat.platform.common.persistence.annotation.MyBatisDao;
import com.boat.platform.test.entity.TestData;

/**
 * 单表生成DAO接口
 * @author ThinkGem
 * @version 2015-04-06
 */
@MyBatisDao
public interface TestDataDao extends CrudDao<TestData> {
	
}